import React from 'react';
import { Link } from 'react-router-dom';
import api from '../services/api';
import '../assets/components/enterprise-card.scss';

const EnterpriseCard = ({ enterprise }) => (
  <Link to={`enterprise/${enterprise.id}`} style={{ textDecoration: 'none' }}>
    <div className="enterprise-card">
      <img src={`${api.defaults.baseURL}/../..${enterprise.photo}`} alt={enterprise.enterprise_name} />
      <div className="base-details">
        <h3 className="title">{enterprise.enterprise_name}</h3>
        <h4 className="subtitle">{enterprise.enterprise_type.enterprise_type_name }</h4>
        <div className="location caption">{`${enterprise.city} - ${enterprise.country}`}</div>
      </div>
    </div>
  </Link>
);

export default EnterpriseCard;
