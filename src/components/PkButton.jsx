import React from 'react';
import '../assets/components/pk-button.scss';

const PkButton = ({
  children, className, color, onClick,
}) => (
  <button onClick={onClick} className={['pk-button', className, `bg-${color}`].join(' ')} type="button">
    {children}
  </button>
);

export default PkButton;
