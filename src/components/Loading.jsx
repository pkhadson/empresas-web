import React from 'react';
import '../assets/components/loading.scss';

const Loading = () => (
  <div className="pk-loading">
    <img src="../assets/loading.svg" alt="loading" />
  </div>
);

export default Loading;
