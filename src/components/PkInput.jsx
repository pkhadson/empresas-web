import React from 'react';
import PropTypes from 'prop-types';
import '../assets/components/pk-input.scss';

const PkInput = ({
  icon, placeholder, validateType, validateText, type, children, value, onInput, size,
}) => (
  <div className={[
    'pk-input',
    size || '',
    validateType
      ? `validate-${validateType}`
      : null].join(' ')}
  >
    {icon && <img src={`/assets/icons/${icon}.svg`} alt="oi" width="26px" className="mr-2 ml-1" />}
    <input {...{ placeholder, type }} className="my-1" value={value} onInput={(e) => onInput(e.target.value)} />
    <span className="pk-input--validate-text">{validateText}</span>
    {children && <div className="right-icon mx-1">{children}</div> }
  </div>
);
PkInput.propTypes = {
  icon: PropTypes.string.isRequired,
};

export default PkInput;
