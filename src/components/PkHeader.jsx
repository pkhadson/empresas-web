import React, { useState } from 'react';
import { connect } from 'react-redux';
import '../assets/components/pk-header.scss';
import { useHistory, useLocation } from 'react-router-dom';
import PkInput from './PkInput';
import mapDispatchToProps from '../utils/mapDispatchToProps';

import { submitSearch as actionSubmitSearch } from '../actions/search';

const PkHeader = ({ dispatch, submitSearch, search }) => {
  const [query, _setQuery] = useState('');
  const [showSearch, _setShowSearch] = useState(!!query);
  console.log(!!query, 'patrick');
  const location = useLocation();
  const history = useHistory();

  const setShowSearch = (v) => {
    _setShowSearch(v);
    if (v) { setTimeout(() => document.querySelector('.base-search input').focus(), 500); }
  };

  const setQuery = (v) => {
    dispatch({ type: 'SEARCH/QUERY', payload: v });
    _setQuery(v);
    submitSearch(v);
  };

  return (
    <div className={`pk-header ${showSearch ? 'open-search' : ''}`}>
      {location.pathname.startsWith('/enterprise/') ? (
        <>
          <div className="spacer">
            <button type="button" onClick={() => history.push('/')}>
              <img src="/assets/icons/arrow-left.svg" alt="Voltar" width="60px" />
            </button>
          </div>
          <div className="page-title">{search.enterprise ? search.enterprise.enterprise_name : ''}</div>
        </>
      ) : (
        <>
          <div className="spacer" />
          <div className="base-logo">
            <img src="../assets/logo-nav.png" alt="logo" />
          </div>
          <div className="base-search">
            <button className="base-input" type="button" onClick={() => !showSearch && setShowSearch(true)}>
              <PkInput
                icon="search"
                size="xl"
                value={query}
                onInput={(v) => setQuery(v)}
                placeholder="Pesquisar"
              >
                <button
                  type="button"
                  onClick={() => {
                    setShowSearch(false);
                    setQuery('');
                  }}
                >
                  <img src="../assets/icons/x.svg" alt="fechar" />
                </button>
              </PkInput>
            </button>
          </div>
        </>
      ) }
    </div>
  );
};

const mapStateToProps = ({ search }) => ({ search });

export default connect(mapStateToProps, mapDispatchToProps([['submitSearch', actionSubmitSearch]]))(PkHeader);
