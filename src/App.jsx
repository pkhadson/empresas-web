import React from 'react';
import { connect } from 'react-redux';
import Routes from './Routes';
import './assets/utils/index.scss';
import Loading from './components/Loading';

const App = ({ store }) => (
  <>
    <Routes />
    { store.loading ? <Loading /> : null }
  </>
);

export default connect(({ store }) => ({ store }))(App);
