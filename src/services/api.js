import axios from 'axios';
import { store } from '../store';

const api = axios.create({
  baseURL: 'https://empresas.ioasys.com.br/api/v1',
});

api.interceptors.request.use((config) => {
  if (!config.headers['Content-Type']) {
    config.headers['Content-Type'] = 'application/json';
  }

  const { auth } = store.getState();
  if (
    auth.credentials
    && auth.credentials.client
    && auth.investor.email
     && auth.credentials.accessToken) {
    config.headers.client = auth.credentials.client;
    config.headers.uid = auth.investor.email;
    config.headers['access-token'] = auth.credentials.accessToken;
  }
  return config;
});

api.interceptors.response.use(
  async (response) => response, async (error) => {
    if (error.response) {
      if (error.response.status === 401
        && !error.request.responseURL.endsWith('sign_in')) {
        window.location = '/login';
      }
    }
    return Promise.reject(error);
  },
);

export default api;
