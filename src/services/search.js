import api from './api';

export const search = (v) => new Promise((r) => {
  api.get('enterprises', { query: { name: v } })
    .then(({ data }) => r({ results: data.enterprises }))
    .catch(() => r({ results: [] }));
});

export const getEnterprise = (v) => new Promise((r) => {
  api.get(`enterprises/${v}`)
    .then(({ data }) => r(data.enterprise))
    .catch(() => r(null));
});

export default { search, getEnterprise };
