import api from './api';

export const signIn = ({ email, password }) => new Promise((r, rj) => {
  api.post('/users/auth/sign_in', { email, password })
    .then(({ data, headers }) => r({
      investor: data.investor,
      credentials: {
        client: headers.client,
        accessToken: headers['access-token'],
      },
    }))
    .catch(({ response }) => {
      if (response) {
        if (response.status === 401
          && response.data.errors.includes('Invalid login credentials. Please try again.')) {
          return rj(new Error('INVALID_CREDENTIALS'));
        }
      }
      return rj(new Error('UNKNOWN_ERROR'));
    });
});

export default { signIn };
