const mapDispatchToProps = (actions) => (dispatch) => actions.reduce((res, it) => ({
  ...res,
  [it[0] || it.name]: (paylaod, callback) => (it[1] || it)(dispatch, paylaod, callback),
}), { dispatch });
export default mapDispatchToProps;
