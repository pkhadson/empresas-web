import React from 'react';
import { connect } from 'react-redux';
import '../assets/views/search.scss';
import EnterpriseCard from '../components/EnterpriseCard';

const Search = ({ search }) => (
  <div className="search-view">
    {search.query.trim() ? (
      <ul>
        { search.results.map((item) => <EnterpriseCard enterprise={item} />) }
      </ul>
    ) : <div className="main-text">Clique na busca para iniciar.</div>}
  </div>
);

const mapStateToProps = ({ search }) => ({ search });

export default connect(mapStateToProps)(Search);
