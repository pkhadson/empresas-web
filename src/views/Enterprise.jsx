import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import api from '../services/api';
import { getEnterprise } from '../services/search';
import '../assets/views/enterprise.scss';

const Enterprise = ({
  dispatch, history, search, match,
}) => {
  useEffect(() => {
    if (search.enterprise != null) return; // previne repetição
    dispatch({ type: 'LOADING', payload: true });
    dispatch({ type: 'SEARCH/ENTERPRISE', payload: 'getting' });
    getEnterprise(match.params.id)
      .then((enterpriseResult) => {
        dispatch({ type: 'LOADING', payload: false });
        if (enterpriseResult) {
          dispatch({ type: 'SEARCH/ENTERPRISE', payload: enterpriseResult });
        } else {
          history.push('/');
        }
      });
  }, []);

  const { enterprise } = search;

  return enterprise
    ? (
      <div className="enterprise-view">
        <img
          src={`${api.defaults.baseURL}/../..${enterprise.photo}`}
          alt={enterprise.enterprise_name}
        />
        <p>{enterprise.description}</p>
      </div>
    ) : <span>sdsd</span>;
};

const mapStateToProps = ({ search }) => ({ search });

export default withRouter(connect(mapStateToProps)(Enterprise));
