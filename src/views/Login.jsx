import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../assets/pages/login.scss';
import PkButton from '../components/PkButton';
import PkInput from '../components/PkInput';
import { signIn as actionSignIn } from '../actions/auth';
import mapDispatchToProps from '../utils/mapDispatchToProps';

const Login = ({ dispatch, signIn, history }) => {
  const [email, _setEmail] = useState('testeapple@ioasys.com.br');
  const [password, _setPassword] = useState('12341234');
  const [showPassword, setShowPassword] = useState(false);
  const [invalid, setInvalid] = useState(false);

  const setEmail = (...args) => {
    setInvalid(false);
    _setEmail(...args);
  };

  const setPassword = (...args) => {
    setInvalid(false);
    _setPassword(...args);
  };

  useEffect(() => { // will mount
    dispatch({ type: 'AUTH/CREDENTIALS', payload: null });
  }, []);

  const methodSignIn = () => {
    signIn({ email, password }, ({ success }) => {
      if (success) {
        history.push('/');
      } else {
        setInvalid(true);
      }
    });
  };

  return (
    <div className="page-login">
      <div className="box-login">
        <img src="../assets/logo-home.png" alt="logo Ioasys" className="mb-16" />
        <h1>
          BEM-VINDO AO
          <br />
          EMPRESAS
        </h1>
        <p className="mt-5">
          Lorem ipsum dolor sit amet, contetur
          <br />
          adipiscing elit. Nunc accumsan.
        </p>
        <div className="mt-8">
          <PkInput
            placeholder="E-mail"
            icon="email"
            value={email}
            onInput={setEmail}
            validateType={invalid ? 'danger' : 'none'}
          >
            {invalid ? <img draggable="false" src="../assets/icons/alert-danger.png" alt="alert danger" /> : null}
          </PkInput>
        </div>
        <div className="mt-8">
          <PkInput
            placeholder="Senha"
            icon="lock"
            type={showPassword ? 'text' : 'password'}
            value={password}
            onInput={setPassword}
            validateType={invalid ? 'danger' : 'none'}
          >

            {invalid
              ? <img draggable="false" src="../assets/icons/alert-danger.png" alt="alert danger" />
              : (
                <button
                  type="button"
                  onMouseDown={() => setShowPassword(true)}
                  onTouchStart={() => setShowPassword(true)}
                  onTouchEnd={() => setShowPassword(true)}
                  onMouseUp={() => setShowPassword(false)}
                  onMouseOut={() => setShowPassword(false)}
                  onBlur={() => setShowPassword(false)}
                >
                  <img src="../assets/eye.jpg" alt="Ver senha" width="25px" />
                </button>
              ) }
          </PkInput>
        </div>
        <p className="text-alert-danger mt-4">{ invalid ? 'Credenciais informadas são inválidas, tente novamente.' : '\u00A0'}</p>
        <div className="mt-4">
          <PkButton
            className="w-full"
            onClick={methodSignIn}
          >
            ENTRAR

          </PkButton>
        </div>
      </div>
    </div>
  );
};

// return { ...res, signIn: (payload) => actionSignIn(dispatch, payload) };
/* const mapDispatchToProps = (dispatch) => [actionSignIn].reduce((res, it) => ({
  ...res,
  [it.name]: (paylaod) => it(dispatch, paylaod),
}), {}); */

export default connect(null, mapDispatchToProps([actionSignIn]))(Login);
