import { combineReducers } from 'redux';
import auth from './auth';
import business from './business';
import store from './store';
import search from './search';

const rootReducer = combineReducers({
  auth, business, store, search,
});

export default rootReducer;
