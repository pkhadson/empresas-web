const INITIAL = {
  query: '',
  results: [],
  enterprise: null,
};

const search = (state = INITIAL, action) => {
  switch (action.type) {
    case 'SEARCH/QUERY': return { ...state, query: action.payload };
    case 'SEARCH/RESULTS': return { ...state, results: action.payload };
    case 'SEARCH/ENTERPRISE': return { ...state, enterprise: action.payload };
    default:
      break;
  }

  return state;
};
export default search;
