const INITIAL = {
  loading: false,
  pageTitle: null,
};

const root = (state = INITIAL, action) => {
  switch (action.type) {
    case 'LOADING':
      if (state.loading !== action.payload) {
        return { ...state, loading: action.payload };
      }
      break;
    case 'PAGETITLE': return { ...state, pageTitle: action.payload };
    default:
      break;
  }

  return state;
};

export default root;
