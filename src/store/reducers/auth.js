const INITIAL = {
  investor: null,
  credentials: null,
  rehydrate: null,
};

const auth = (state = INITIAL, action) => {
  console.log(action.type);
  if (action.type === 'persist/REHYDRATE' && action.payload) return { ...state, ...action.payload.auth, rehydrate: true };
  switch (action.type) {
    case 'persist/REHYDRATE': return {
      ...state,
      ...action.payload ? action.payload.auth : {},
      rehydrate: true,
    };
    case 'AUTH/INVESTOR': return { ...state, investor: action.payload };
    case 'AUTH/CREDENTIALS': return { ...state, credentials: action.payload };
    default:
      break;
  }

  console.log('oi');

  return state;
};

export default auth;
