import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createStore } from 'redux';
import rootReducer from './reducers';

const persistedReducer = persistReducer({
  key: 'ioasys-empresas',
  storage,
  whitelist: ['auth'],
}, rootReducer);

const store = createStore(persistedReducer);
const persistor = persistStore(store);

export { store, persistor };
