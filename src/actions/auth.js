import { signIn as serviceSignIn } from '../services/auth';

export const signIn = async (dispatch, payload, callback) => {
  dispatch({ type: 'LOADING', payload: true });

  await serviceSignIn(payload || {})
    .then(({ investor, credentials }) => {
      dispatch({ type: 'LOADING', payload: false });
      dispatch({ type: 'AUTH/INVESTOR', payload: investor });
      dispatch({ type: 'AUTH/INVESTOR', payload: investor });
      dispatch({ type: 'AUTH/CREDENTIALS', payload: credentials });
      callback({ success: true });
    })
    .catch((error) => {
      dispatch({ type: 'LOADING', payload: false });
      if (callback) callback({ success: false, message: error.message });
    });
};
export default { signIn };
