import { debounce } from 'lodash';
import { search as serviceSearch } from '../services/search';

export const submitSearch = debounce((dispatch, payload) => {
  serviceSearch(payload)
    .then(({ results }) => dispatch({
      type: 'SEARCH/RESULTS',
      payload: results,
    }));
}, 500);

export default { submitSearch };
