import React from 'react';
import PkHeader from '../../components/PkHeader';
import '../../assets/layout/main.scss';

const Main = ({ children }) => (
  <div className="layout-main">
    <PkHeader />
    {children}
  </div>
);

export default Main;
