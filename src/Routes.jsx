import React from 'react';
import { connect } from 'react-redux';
import {
  Route, BrowserRouter, Switch, Redirect,
} from 'react-router-dom';
import Main from './layouts/Main';
import Login from './views/Login';
import Search from './views/Search';
import Enterprise from './views/Enterprise';

const PrivateRoute = connect(
  ({ auth }) => ({ auth }),
)(
  ({
    auth, children, ...rest
  }) => (
    <Route
      {...rest}
      component={(props) => (
        (auth.credentials && auth.investor)
          ? children
          : auth.rehydrate && <Redirect to={{ pathname: '/login', state: props.location }} />
      )}
    />
  ),
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/login" component={Login} />
      <PrivateRoute path="/">
        <Main>
          <Switch>
            <Route path="/" exact><Search /></Route>
            <Route path="/enterprise/:id"><Enterprise /></Route>
          </Switch>
        </Main>
      </PrivateRoute>
    </Switch>
  </BrowserRouter>
);

export default Routes;
