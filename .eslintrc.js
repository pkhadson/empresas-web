module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'eslint:recommended',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    '@eslint/no-explicit-any': 'off',
    '@eslint/ban-ts-ignore': 'off',
    'react/prop-types': 0,
    'react/jsx-props-no-spreading': 0,
    'no-param-reassign': 0,
  },
};
