# Primeiros passos
1. Fiz um fork do repostório original
2. Fiz o start do projeto com o comando: `npx create-react-app empresas web`
3. Fiz algumas mudanças na estruturação inicial
4. Habilitei o ESLint/Prettier do VSCode
5. Fiz a quebra de todas as tarefas no Trello

# Desenvolvimento
1. Após a preparação do ambiente eu iniciei criando o componente de texto
2. Fiz a criação do botão
3. Criei o serviço e a tela de login

<div style="background: #ffff0030;padding: 10px;border-radius: 5px;border: solid 1px yellow">
    Na criação do componente de botão foi necessário usar uma fonte diferente ao protótipo
</div>